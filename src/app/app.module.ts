import { NgModule, ErrorHandler } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule, Http } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, DeepLinkConfig } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Page } from "../pages/page/page";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateModule, TranslateStaticLoader, TranslateLoader } from "ng2-translate";

import { ComponentsModule, MapfreErrorHandler } from 'mapfre-framework';

export const deepLinkConfig: DeepLinkConfig = {
  links: [
    { component: HomePage, name: 'HomePage', segment: 'home' },
    { component: Page, name: 'PageTwo', segment: 'page2' }
  ]
};

export function createTranslateLoader(http: Http): TranslateStaticLoader {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Page
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      locationStrategy: 'hash'
    }, deepLinkConfig),
    HttpModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    }),
    ComponentsModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Page
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: MapfreErrorHandler },
    { provide: APP_BASE_HREF, useValue: '/' }
  ]
})
export class AppModule { }
