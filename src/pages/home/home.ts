import { Component } from '@angular/core';
import { NavController } from "ionic-angular";
import { Page } from "../page/page";
import { BasePage } from 'mapfre-framework';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  name: string = "HomePage";

  constructor(
    public nav: NavController
  ) { }

  goToPage(): void {
    this.onLogin();
    // if (window.visualizarPaso) {
    //   window.visualizarPaso({
    //     type: 'event',
    //     evtCategory: "Button",
    //     evtAction: "Click",
    //     evtLabel: "goToPage2"
    //   });
    // }
    this.nav.push(Page);
  }

  displayNotification(): void {
    if (Notification['permission'] == 'granted') {
      navigator.serviceWorker.getRegistration().then(function(reg) {
        var options = {
          body: 'Esto es un ejemplo de notificación!!',
          icon: 'assets/imgs/logo.png',
          vibrate: [100, 50, 100],
          data: {
            dateOfArrival: Date.now(),
            primaryKey: 1
          }
        };
        reg.showNotification('Hola!', options);
      });
    }
  }

  onLogin(): void {
    //window.datalayer.idUser = 'exampleUserId';
  }

}
